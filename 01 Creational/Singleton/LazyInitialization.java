
public class LazyInitialization {
	
	//Early instance not created at load time
	private static LazyInitialization lazyInstance = null;
	
	//private constructor
	private LazyInitialization() {
		
	}
	
	public static LazyInitialization getInstance() {
		if(lazyInstance == null) {
			synchronized (LazyInitialization.class) {
				lazyInstance = new LazyInitialization();
			}
		}
		return lazyInstance;
	}
}

/* Drawback: If two threads create the instance and check if "instance == null" & both instances identify 
 * it as null, they sequentially go into a synchronized block and create the instance. We end up with two
 * instances in our application
 */
