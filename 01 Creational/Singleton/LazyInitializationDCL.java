
//DCL is "Double Check Lock" 
public class LazyInitializationDCL {
	// Early instance not created at load time
	//volatile is must for DCL or whole application will crash
	private static volatile LazyInitializationDCL lazyInstanceDCL = null; 

	// private constructor
	private LazyInitializationDCL() {

	}

	public static LazyInitializationDCL getInstance() {
		if (lazyInstanceDCL == null) {
			synchronized (LazyInitializationDCL.class) {
				//Double Check Lock
				if(lazyInstanceDCL == null) {
					lazyInstanceDCL = new LazyInitializationDCL();
				}
			}
		}
		return lazyInstanceDCL;
	}
}
