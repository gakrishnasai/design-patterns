
public class EagerInitialization {
	
	//Early instance created at load time
    private static EagerInitialization eagerInstance = new EagerInitialization();
 
    // private constructor
    private EagerInitialization() {
    }
 
    public static EagerInitialization getInstance() {
        return eagerInstance;
    }
}